## Documentation

[Postman documentation](https://documenter.getpostman.com/view/19782312/2s84Diy57z)

### Swagger documentation

- [Swagger Documentation](http://localhost:6432/api/swagger-ui/index.html) incase of running with jar
- [Swagger Documentation](http://localhost:6444/api/swagger-ui/index.html) incase of running with docker compose

## Technologies
- Springboot
- Maven
- JPA (Hibernate)
- PostgreSQL
- H2
- Hibernate Envers
- Bean validation (Hibernate Validator)
- Swagger
- Docker

## Build

Package the project with this command from root directory

```bash
mvn clean package
```

Build docker image with this command from root directory

```bash
docker build -t mahmoud/drone-delivery-api:0.0.1.RELEASE .
```

## Test

run test with maven test goal from root directory

```bash
mvn test
```

## Run

### Run without container

Run PostgreSQL from any directory

```bash
docker run -e POSTGRES_USER=drone-db -e POSTGRES_PASSWORD=drone-db -p 5432:5432 -v /data:/var/lib/postgresql/data --name postgresql postgres
```

Run jar file

run from target directory
- Prerequisites

  > Build by maven

```bash
 java -jar drone-delivery-api.jar
```

### Run with docker-compose

- Prerequisites

  > Build by maven

  > Build docker image
  
then run from root directory

```bash
docker-compose up
```
