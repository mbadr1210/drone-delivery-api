package com.mbadr.drone.delivery.api.service.Impl;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import com.mbadr.drone.delivery.api.entity.Image;
import com.mbadr.drone.delivery.api.entity.Medication;
import com.mbadr.drone.delivery.api.entity.Model;
import com.mbadr.drone.delivery.api.entity.State;
import com.mbadr.drone.delivery.api.mapper.DroneMapper;
import com.mbadr.drone.delivery.api.mapper.MedicationMapper;
import com.mbadr.drone.delivery.api.repository.DroneRepository;
import com.mbadr.drone.delivery.api.service.MedicationService;
import com.mbadr.drone.delivery.api.service.exceptions.ConflictUpdateException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceDuplicationException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import java.util.Optional;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {DroneServiceImpl.class})
@ExtendWith(SpringExtension.class)
class DroneServiceImplTest {
    @MockBean
    private DroneMapper droneMapper;

    @MockBean
    private DroneRepository droneRepository;

    @Autowired
    private DroneServiceImpl droneServiceImpl;

    @MockBean
    private MedicationMapper medicationMapper;

    @MockBean
    private MedicationService medicationService;

    /**
     * Method under test: {@link DroneServiceImpl#findBySerialNumber(String)}
     */
    @Test
    @DisplayName("find by serial number success")
    void testFindBySerialNumberSuccess() {
        Drone drone = new Drone();
        drone.setBatteryCapacity(1);
        drone.setId(123L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setSerialNumber("42");
        drone.setState(State.IDLE);
        drone.setWeightLimit(3);
        Optional<Drone> ofResult = Optional.of(drone);

        when(droneRepository.findBySerialNumber((String) any())).thenReturn(ofResult);

        droneServiceImpl.findBySerialNumber("42");

        verify(droneRepository).findBySerialNumber((String) any());
    }

    /**
     * Method under test: {@link DroneServiceImpl#findBySerialNumber(String)}
     */
    @Test
    @DisplayName("find by serial number notfound")
    void testFindBySerialNumberFailure() {
        when(droneRepository.findBySerialNumber((String) any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> droneServiceImpl.findBySerialNumber("42"));
        verify(droneRepository).findBySerialNumber((String) any());
        }


    /**
     * Method under test: {@link DroneServiceImpl#getLoadedMedications(String)}
     */
    @Test
    @DisplayName("get loaded medications for not found drone")
    void testGetLoadedMedicationsNotFoundDrone() {
        when(droneRepository.findBySerialNumber((String) any())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> droneServiceImpl.getLoadedMedications("42"));
        verify(droneRepository).findBySerialNumber((String) any());
    }


    /**
     * Method under test: {@link DroneServiceImpl#loadMedications(List, String)}
     */
    @Test
    @DisplayName("load medication with invalid state not (LOADING - LOADED)")
    void testLoadMedicationsWithNotLOADINGState() {
        Drone drone = mock(Drone.class);

        when(drone.getState()).thenReturn(State.IDLE);
        doNothing().when(drone).loadMedication((Medication) any());
        doNothing().when(drone).setBatteryCapacity((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerialNumber((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeightLimit((Integer) any());
        when(drone.getState()).thenReturn((State) any());

        drone.setBatteryCapacity(1);
        drone.setId(123L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setSerialNumber("42");
        drone.setState(State.IDLE);
        drone.setWeightLimit(3);
        Optional<Drone> ofResult = Optional.of(drone);

        when(droneRepository.findBySerialNumber((String) any())).thenReturn(ofResult);

        assertThrows(ConflictUpdateException.class, () -> droneServiceImpl.loadMedications(new ArrayList<>(), "42"));
        verify(droneRepository).findBySerialNumber((String) any());
    }

    /**
     * Method under test: {@link DroneServiceImpl#registerDrone(DroneSummaryDTO)}
     */
    @Test
    @DisplayName("when registering existed drone - throws ResourceDuplicationException")
    void testRegisterDrone() {
        DroneRepository droneRepository = mock(DroneRepository.class);
        when(droneRepository.existsBySerialNumber((String) any())).thenReturn(true);
        DroneServiceImpl droneServiceImpl = new DroneServiceImpl(droneRepository);

        assertThrows(ResourceDuplicationException.class, () -> droneServiceImpl.registerDrone(new DroneSummaryDTO()));
        verify(droneRepository).existsBySerialNumber((String) any());
    }

    /**
     * Method under test: {@link DroneServiceImpl#registerDrone(DroneSummaryDTO)}
     */
    @Test
    @DisplayName("when registering new drone - successful")
    void testRegisterDroneSuccess() {
        // when
        Drone drone = new Drone();
        drone.setId(1L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setBatteryCapacity(50);
        drone.setSerialNumber("serial");
        drone.setWeightLimit(400);
        drone.setState(State.IDLE);

        when(droneRepository.existsBySerialNumber((String) any())).thenReturn(false);
        when(droneRepository.save(any())).thenReturn(drone);

        // then
        DroneSummaryDTO droneSummaryDTO = new DroneSummaryDTO();
        droneSummaryDTO.setModel(Model.LIGHT_WEIGHT);
        droneSummaryDTO.setBatteryCapacity(50);
        droneSummaryDTO.setSerialNumber("serial");
        droneSummaryDTO.setWeightLimit(400);
        droneSummaryDTO.setState(State.LOADING);

        Drone created = droneServiceImpl.registerDrone(droneSummaryDTO);

        // assert
        verify(droneRepository).existsBySerialNumber((String) any());
        assertSame(drone, created);
    }


    /**
     * Method under test: {@link DroneServiceImpl#setToBeLoaded(String)}
     */
    @Test
    @DisplayName("when setToBeLoaded with IDLE - Success")
    void testSetToBeLoadedSuccess() {
        Drone drone = mock(Drone.class);
        doNothing().when(drone).loadMedication((Medication) any());
        doNothing().when(drone).setBatteryCapacity((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerialNumber((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeightLimit((Integer) any());
        when(drone.getBatteryCapacity()).thenReturn(40);
        when(drone.getState()).thenReturn(State.IDLE);

        drone.setBatteryCapacity(40);
        drone.setId(123L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setSerialNumber("42");
        drone.setState(State.IDLE);
        drone.setWeightLimit(3);

        Optional<Drone> ofResult = Optional.of(drone);
        when(droneRepository.findBySerialNumber((String) any())).thenReturn(ofResult);

        droneServiceImpl.setToBeLoaded("drone");

        verify(droneRepository).save((Drone) any());
        verify(droneRepository).findBySerialNumber((String) any());
        verify(drone).getState();
        verify(drone, atLeast(1)).setState((State) any());
    }

    @Test
    @DisplayName("when setToBeLoaded with low battery - Fail")
    void testSetToBeLoadedBatteryFailure() {
        Drone drone = new Drone();

        drone.setBatteryCapacity(20);
        drone.setId(123L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setSerialNumber("42");
        drone.setState(State.IDLE);
        drone.setWeightLimit(3);

        Optional<Drone> ofResult = Optional.of(drone);
        when(droneRepository.findBySerialNumber((String) any())).thenReturn(ofResult);

        assertThrows(ConflictUpdateException.class, () -> droneServiceImpl.setToBeLoaded("drone"));

        verify(droneRepository).findBySerialNumber((String) any());
    }

    @Test
    @DisplayName("when setToBeLoaded with low battery - Fail")
    void testSetToBeLoadedStateFailure() {
        Drone drone = mock(Drone.class);
        doNothing().when(drone).loadMedication((Medication) any());
        doNothing().when(drone).setBatteryCapacity((Integer) any());
        doNothing().when(drone).setId((Long) any());
        doNothing().when(drone).setModel((Model) any());
        doNothing().when(drone).setSerialNumber((String) any());
        doNothing().when(drone).setState((State) any());
        doNothing().when(drone).setWeightLimit((Integer) any());
        when(drone.getBatteryCapacity()).thenReturn(40);
        when(drone.getState()).thenReturn(any());

        drone.setBatteryCapacity(40);
        drone.setId(123L);
        drone.setModel(Model.LIGHT_WEIGHT);
        drone.setSerialNumber("42");
        drone.setState(State.IDLE);
        drone.setWeightLimit(3);

        Optional<Drone> ofResult = Optional.of(drone);
        when(droneRepository.findBySerialNumber((String) any())).thenReturn(ofResult);

        assertThrows(ConflictUpdateException.class, () -> droneServiceImpl.setToBeLoaded("drone"));

        verify(droneRepository).findBySerialNumber((String) any());
        verify(drone, atLeast(2)).getState();
        verify(drone, atLeast(1)).setState((State) any());
    }

}

