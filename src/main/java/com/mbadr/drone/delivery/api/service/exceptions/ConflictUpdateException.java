package com.mbadr.drone.delivery.api.service.exceptions;

import java.util.Map;

public class ConflictUpdateException extends RuntimeException {
    private final String message;
    private final Map<String, String> errors;

    public ConflictUpdateException(String message, Map<String, String> errors) {
        this.message = message;
        this.errors = errors;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}
