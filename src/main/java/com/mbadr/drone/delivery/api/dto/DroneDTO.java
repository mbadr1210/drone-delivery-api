package com.mbadr.drone.delivery.api.dto;

import java.util.ArrayList;
import java.util.List;

public class DroneDTO extends DroneSummaryDTO {
    private List<MedicationDTO> medications = new ArrayList<>();

    public List<MedicationDTO> getMedications() {
        return medications;
    }

    public void setMedications(List<MedicationDTO> medications) {
        this.medications = medications;
    }
}
