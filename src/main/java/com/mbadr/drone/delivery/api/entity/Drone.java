package com.mbadr.drone.delivery.api.entity;

import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.service.exceptions.ConflictUpdateException;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Entity
@DynamicUpdate
@Audited
@NamedQueries({@NamedQuery(
        name = Drone.FIND_ALL_DRONE_SUMMARIES,
        query = Drone.FIND_ALL_DRONE_SUMMARIES_QUERY)})
public class Drone {

    public static final String FIND_ALL_DRONE_SUMMARIES_QUERY =
            " select new com.mbadr.drone.delivery.api.dto.DroneSummaryDTO(d) " +
            " from Drone d where d.state = :state";
    public static final String FIND_ALL_DRONE_SUMMARIES = "Drone.droneSummariesByState";
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @Column(unique = true, updatable = false, nullable = false, length = 100)
    private String serialNumber;
    @Column(nullable = false, updatable = false)
    private Integer weightLimit;
    @Column
    private Integer batteryCapacity;
    @Enumerated(EnumType.STRING)
    private Model model;
    @Enumerated(EnumType.STRING)
    private State state;

    @Transient
    private Integer availableWeight;

    @OneToMany(mappedBy = "drone", cascade = CascadeType.ALL)
    @NotAudited
    private final Set<Medication> medications = new HashSet<>();

    public Drone() {
    }

    public Drone(DroneSummaryDTO droneSummaryDTO) {
        this.serialNumber = droneSummaryDTO.getSerialNumber();
        this.state = droneSummaryDTO.getState();
        this.weightLimit = droneSummaryDTO.getWeightLimit();
        this.batteryCapacity = droneSummaryDTO.getBatteryCapacity();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Integer weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        if (State.LOADING.equals(state) && batteryCapacity < 25)
            throw new ConflictUpdateException("Drone can't be loaded with low battery",
                    new HashMap<String, String>() {{
                        put("state", "can't load drone with low battery");
                    }}
            );
        this.state = state;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Set<Medication> getMedications() {
        return Collections.unmodifiableSet(medications);
    }

    public void loadMedication(Medication medication) {
        if (medication.getWeight() <= availableWeight) {
            if (medications.add(medication)) {
                medication.setDrone(this);
                availableWeight -= medication.getWeight();
            }
        }
    }

    @PostLoad
    private void calculateAvailableWeight() {
        int medicationWeightSum = this.medications.stream()
                .mapToInt(Medication::getWeight)
                .sum();
        this.availableWeight = this.weightLimit - medicationWeightSum;
    }

    @Override
    public String toString() {
        return "Drone{" +
                ", serialNumber='" + serialNumber + '\'' +
                ", weightLimit=" + weightLimit +
                ", batteryCapacity=" + batteryCapacity +
                ", state=" + state +
                '}';
    }
}
