package com.mbadr.drone.delivery.api.repository;

import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import com.mbadr.drone.delivery.api.entity.State;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface DroneRepository extends JpaRepository<Drone, Long> {

    boolean existsBySerialNumber(String serialNumber);

    Optional<Drone> findBySerialNumber(String serialNumber);

    List<DroneSummaryDTO> droneSummariesByState(State state, PageRequest pageRequest);
}
