package com.mbadr.drone.delivery.api.mapper;

import com.mbadr.drone.delivery.api.dto.ImageSummaryDTO;
import com.mbadr.drone.delivery.api.entity.Image;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ImageMapper {

    ImageSummaryDTO toDTO(Image image);

    Image toEntity(ImageSummaryDTO imageSummaryDTO);
}
