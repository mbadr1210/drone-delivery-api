package com.mbadr.drone.delivery.api.controller;

import com.mbadr.drone.delivery.api.dto.ImageResponse;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Medication;
import com.mbadr.drone.delivery.api.mapper.MedicationMapper;
import com.mbadr.drone.delivery.api.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping("medications")
public class MedicationController {

    private final MedicationService medicationService;
    private MedicationMapper medicationMapper;

    @Autowired
    public void setMedicationMapper(MedicationMapper medicationMapper) {
        this.medicationMapper = medicationMapper;
    }

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @PostMapping
    public ResponseEntity<?> registerMedication(@RequestBody @Valid MedicationDTO medicationDTO) {
        Medication response = medicationService.registerMedication(medicationDTO);
        return ResponseEntity.created(
                ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{code}")
                        .buildAndExpand(response.getCode())
                        .toUri()
        ).build();
    }

    @GetMapping("{code}")
    public ResponseEntity<?> findByCode(@PathVariable String code) {
        MedicationDTO response = medicationMapper.toDTO(medicationService.findByCode(code));
        return ResponseEntity.ok(response);
    }

    @PostMapping("{code}/images")
    public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file,
                                         @PathVariable String code) {
        return ResponseEntity.status(HttpStatus.OK)
                .header("location",
                        ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("")
                        .toUriString())
                .body(medicationService.uploadImage(file, code));
    }

    @GetMapping("{code}/images")
    public ResponseEntity<byte[]> downloadMedicationImage(@PathVariable String code) {
        ImageResponse imageResponse = medicationService.getMedicationImage(code);
        return ResponseEntity.ok()
                .contentType(imageResponse.getMediaType())
                .body(imageResponse.getBytes());
    }
}
