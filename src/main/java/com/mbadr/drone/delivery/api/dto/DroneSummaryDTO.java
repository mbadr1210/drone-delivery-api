package com.mbadr.drone.delivery.api.dto;


import com.mbadr.drone.delivery.api.entity.Drone;
import com.mbadr.drone.delivery.api.entity.Model;
import com.mbadr.drone.delivery.api.entity.State;

import javax.validation.constraints.*;

public class DroneSummaryDTO {

    public static final String DRONE = "Drone";
    @NotNull
    private String serialNumber;
    @Max(value = 500, message = "must be less than 500")
    @Positive
    private Integer weightLimit;
    @Max(value = 100, message = "must be less than 100")
    private Integer batteryCapacity;
    private State state;
    private Model model;

    public DroneSummaryDTO() {
    }

    public DroneSummaryDTO(String serialNumber, Integer weightLimit, Integer batteryCapacity, State state, Model model) {
        this.serialNumber = serialNumber;
        this.weightLimit = weightLimit;
        this.batteryCapacity = batteryCapacity;
        this.state = state;
        this.model = model;
    }

    public DroneSummaryDTO(Drone drone) {
        this.serialNumber = drone.getSerialNumber();
        this.weightLimit = drone.getWeightLimit();
        this.batteryCapacity = drone.getBatteryCapacity();
        this.state = drone.getState();
        this.model = drone.getModel();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Integer weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
