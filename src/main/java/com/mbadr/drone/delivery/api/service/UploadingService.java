package com.mbadr.drone.delivery.api.service;

import com.mbadr.drone.delivery.api.dto.ImageSummaryDTO;
import com.mbadr.drone.delivery.api.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface UploadingService {

    void upload(MultipartFile imageMultiPart, Image image) throws IOException;

    byte[] download(Image image) throws IOException;

    ImageSummaryDTO saveImage(Image image);
}
