package com.mbadr.drone.delivery.api.entity;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

public class ImageFactory {
    public static Image buildImage(MultipartFile file) {
        Image image = new Image();
        image.setSize(file.getSize());
        image.setFileType(getFileType(file));
        return image;
    }
    private static Map<String, String> contentTypeToFileType = new HashMap<>();
    private static Map<String, MediaType> fileTypeToContentType = new HashMap<>();
    static {
        contentTypeToFileType.put(MediaType.IMAGE_JPEG_VALUE, "jpg");
        contentTypeToFileType.put(MediaType.IMAGE_PNG_VALUE, "png");
        fileTypeToContentType.put("jpg", MediaType.IMAGE_JPEG);
        fileTypeToContentType.put("png", MediaType.IMAGE_PNG);
    }
    public static MediaType getMediaType(String fileType) {
        return fileTypeToContentType.get(fileType);
    }
    private static String getFileType(MultipartFile file) {
        return contentTypeToFileType.get(file.getContentType());
    }
}
