package com.mbadr.drone.delivery.api.mapper;

import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Medication;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MedicationMapper {

    MedicationDTO toDTO(Medication medication);

    Medication toEntity(MedicationDTO medicationDTO);
}
