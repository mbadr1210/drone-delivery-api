package com.mbadr.drone.delivery.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * configuration for Swagger2
 * @author mbadr
 */
@Configuration
public class SwaggerConfiguration {
    /** List of Swagger endpoints (used by {@code SecurityConfiguration}) */
    public static final String[] SWAGGER_ENDPOINTS = {
            "/v2/api-docs",
            "/v3/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui/",
            "/swagger-ui/index.html",
            "/webjars/**"
    };
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.OAS_30).select()
                .apis(RequestHandlerSelectors.basePackage("com.mbadr.drone.delivery.api")).build();
    }
}
