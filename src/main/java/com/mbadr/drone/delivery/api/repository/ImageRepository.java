package com.mbadr.drone.delivery.api.repository;

import com.mbadr.drone.delivery.api.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
