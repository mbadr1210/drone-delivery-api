package com.mbadr.drone.delivery.api.service.exceptions;

import java.util.HashMap;

public class ImageTransferException extends RuntimeException {
    private String code;
    private HashMap<String, String> errors;

    public ImageTransferException(String code, HashMap<String, String> errors) {
        this.code = code;
        this.errors = errors;
    }

    public String getCode() {
        return code;
    }

    public HashMap<String, String> getErrors() {
        return errors;
    }
}
