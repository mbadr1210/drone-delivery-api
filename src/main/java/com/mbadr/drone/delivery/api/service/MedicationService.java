package com.mbadr.drone.delivery.api.service;

import com.mbadr.drone.delivery.api.dto.ImageResponse;
import com.mbadr.drone.delivery.api.dto.ImageSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Medication;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface MedicationService {

    ImageResponse getMedicationImage(String code);

    Medication registerMedication(MedicationDTO medication);

    ImageSummaryDTO uploadImage(MultipartFile file, String code);

    Medication findByCode(String code);

    List<Medication> findAllByCodeIn(List<String> medicationsCodes);
}
