package com.mbadr.drone.delivery.api.entity;

public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
