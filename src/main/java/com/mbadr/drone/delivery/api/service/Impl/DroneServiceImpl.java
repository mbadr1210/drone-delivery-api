package com.mbadr.drone.delivery.api.service.Impl;

import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import com.mbadr.drone.delivery.api.entity.State;
import com.mbadr.drone.delivery.api.mapper.DroneMapper;
import com.mbadr.drone.delivery.api.mapper.MedicationMapper;
import com.mbadr.drone.delivery.api.repository.DroneRepository;
import com.mbadr.drone.delivery.api.service.DroneService;
import com.mbadr.drone.delivery.api.service.MedicationService;
import com.mbadr.drone.delivery.api.service.exceptions.ConflictUpdateException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceDuplicationException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    @Autowired
    private DroneMapper droneMapper;
    @Autowired
    private MedicationMapper medicationMapper;
    @Autowired
    private MedicationService medicationService;

    public DroneServiceImpl(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Override
    public Drone findBySerialNumber(String serialNumber) {
        return droneRepository.findBySerialNumber(serialNumber)
                .orElseThrow(() -> new ResourceNotFoundException(DroneSummaryDTO.DRONE, serialNumber));
    }

    @Override
    @Transactional(readOnly = true)
    public List<MedicationDTO> getLoadedMedications(String serialNumber) {
        return findBySerialNumber(serialNumber)
                .getMedications()
                .stream()
                .map(medication -> medicationMapper.toDTO(medication))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<DroneSummaryDTO> getAvailableDrones(int page, int pageSize) {
        return droneRepository.droneSummariesByState(State.IDLE, PageRequest.of(page, pageSize));
    }

    @Override
    @Transactional
    public Drone loadMedications(List<String> medicationsCodes, String serialNumber) {
        Drone drone = findBySerialNumber(serialNumber);

        checkEligibilityForBeingLoaded(drone);

        medicationsCodes.forEach(
                medicationsCode -> drone.loadMedication(medicationService.findByCode(medicationsCode))
        );
        drone.setState(State.LOADED);
        droneRepository.save(drone);
        return drone;
    }

    private void checkEligibilityForBeingLoaded(Drone drone) {
        if (!State.LOADING.equals(drone.getState()) && !State.LOADED.equals(drone.getState())) {
            throw new ConflictUpdateException("Drone state conflict.",
                    new HashMap<String, String>() {{
                        put("state", "Drone should be in LOADING state to be LOADED");
                    }});
        }
    }

    @Override
    @Transactional
    public Drone registerDrone(DroneSummaryDTO droneSummaryDTO) {
        if (droneRepository.existsBySerialNumber(droneSummaryDTO.getSerialNumber())) {
            throw new ResourceDuplicationException(DroneSummaryDTO.DRONE, droneSummaryDTO.getSerialNumber());
        }
        Drone drone = droneMapper.toEntity(droneSummaryDTO);
        return droneRepository.save(drone);
    }

    @Override
    @Transactional
    public Drone setToBeLoaded(String serialNumber) {
        Drone drone = findBySerialNumber(serialNumber);
        checkAvailabilityForLoading(drone);
        drone.setState(State.LOADING);
        return droneRepository.save(drone);
    }

    private void checkAvailabilityForLoading(Drone drone) {
        if (!State.IDLE.equals(drone.getState()) && !State.LOADING.equals(drone.getState())) {
            throw new ConflictUpdateException("Drone state conflict.",
                    new HashMap<String, String>() {{
                        put("state", "Drone should be in IDLE state to be assigned for LOADING");
                    }});
        }
    }

    @Override
    @Transactional
    public Drone update(String serialNumber, DroneSummaryDTO droneDTO) {
        Drone drone = findBySerialNumber(serialNumber);

        checkEligibilityForUpdate(droneDTO, drone);
        drone.setBatteryCapacity(droneDTO.getBatteryCapacity());
        drone.setState(droneDTO.getState());
        drone = droneRepository.save(drone);

        return drone;
    }

    private void checkEligibilityForUpdate(DroneSummaryDTO droneDTO, Drone drone) {
        Map<String, String> errors = new HashMap<>();
        if (!drone.getSerialNumber().equals(droneDTO.getSerialNumber()))
            errors.put("serialNumber", "cannot update identity");
        if (!drone.getWeightLimit().equals(droneDTO.getWeightLimit()))
            errors.put("weightLimit", "is not updatable");
        if (!drone.getModel().equals(droneDTO.getModel()))
            errors.put("model", "is not updatable");
        if (!errors.isEmpty())
            throw new ConflictUpdateException("Attempt to update not updatable fields", errors);
    }
}
