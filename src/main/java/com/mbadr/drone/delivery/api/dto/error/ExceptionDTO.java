package com.mbadr.drone.delivery.api.dto.error;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class ExceptionDTO {
    private LocalDateTime timestamp = LocalDateTime.now();
    private String message;
    private Map<String, String> errors = new HashMap<>();

    public Map<String, String> getErrors() {
        return errors;
    }
    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
