package com.mbadr.drone.delivery.api.dto;

import org.springframework.http.MediaType;

public class ImageResponse {

    private MediaType mediaType;
    private byte[] bytes;

    public ImageResponse(MediaType mediaType, byte[] bytes) {
        this.mediaType = mediaType;
        this.bytes = bytes;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
