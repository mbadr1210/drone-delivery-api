package com.mbadr.drone.delivery.api.dto;


import com.mbadr.drone.delivery.api.entity.Medication;

import javax.validation.constraints.Pattern;

public class MedicationDTO {

    public static final String MEDICATION = "Medication";
    @Pattern(regexp = "([A-Za-z]|\\d|-|_)+", message = "must contain only letters, digits, '-' or '_'")
    private String name;
    private Integer weight;
    @Pattern(regexp = "([A-Z]|\\d|_)+", message = "must contain only upper case letters, digits or '_'")
    private String code;

    public MedicationDTO() {
    }

    public MedicationDTO(Medication medication) {
        this.code = medication.getCode();
        this.weight = medication.getWeight();
        this.name = medication.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
