package com.mbadr.drone.delivery.api.mapper;

import com.mbadr.drone.delivery.api.dto.DroneDTO;
import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DroneMapper {

    DroneSummaryDTO toDTO(Drone drone);

    Drone toEntity(DroneSummaryDTO droneSummaryDTO);

    default DroneDTO toLoadedDTO(Drone drone) {
        if ( drone == null ) {
            return null;
        }

        DroneDTO droneDTO = new DroneDTO();

        droneDTO.setSerialNumber( drone.getSerialNumber() );
        droneDTO.setWeightLimit( drone.getWeightLimit() );
        droneDTO.setBatteryCapacity( drone.getBatteryCapacity() );
        droneDTO.setState( drone.getState() );
        droneDTO.setModel( drone.getModel() );
        drone.getMedications().forEach(medication ->
                droneDTO.getMedications().add(new MedicationDTO(medication)));

        return droneDTO;
    }
}
