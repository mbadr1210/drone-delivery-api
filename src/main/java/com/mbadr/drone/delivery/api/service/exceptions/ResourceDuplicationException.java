package com.mbadr.drone.delivery.api.service.exceptions;

public class ResourceDuplicationException extends RuntimeException {

    private String resourceName;
    private String id;

    public ResourceDuplicationException(String resourceName, String id) {
        this.resourceName = resourceName;
        this.id = id;
    }

    @Override
    public String getMessage() {
        return this.getResourceName() + " is already exists";
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getId() {
        return id;
    }
}
