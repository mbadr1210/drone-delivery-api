package com.mbadr.drone.delivery.api.entity;

public enum Model {
    LIGHT_WEIGHT, MIDDLE_WEIGHT, CRUISER_WEIGHT, HEAVY_WEIGHT
}
