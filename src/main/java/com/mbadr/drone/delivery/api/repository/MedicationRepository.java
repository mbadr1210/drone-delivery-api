package com.mbadr.drone.delivery.api.repository;

import com.mbadr.drone.delivery.api.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    boolean existsByCode(String code);
    Optional<Medication> findByCode(String code);
    List<Medication> findAllByCodeIn(Collection<String> code);
}
