package com.mbadr.drone.delivery.api.controller;

import com.mbadr.drone.delivery.api.dto.DroneDTO;
import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import com.mbadr.drone.delivery.api.mapper.DroneMapper;
import com.mbadr.drone.delivery.api.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("drones")
public class DroneController {

    private final DroneService droneService;
    @Autowired
    private DroneMapper droneMapper;

    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping
    public ResponseEntity<?> registerDrone(@RequestBody @Valid DroneSummaryDTO droneSummaryDTO) {
        Drone createdDrone = droneService.registerDrone(droneSummaryDTO);
        return ResponseEntity.created(
                ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{serialNumber}")
                        .buildAndExpand(createdDrone.getSerialNumber())
                        .toUri()
        ).build();
    }

    @GetMapping("{serialNumber}")
    public ResponseEntity<?> findBySerialNumber(@PathVariable String serialNumber) {
        DroneSummaryDTO response = droneMapper.toDTO(droneService.findBySerialNumber(serialNumber));
        return ResponseEntity.ok(response);
    }

    @PutMapping("{serialNumber}/medications")
    public ResponseEntity<?> loadMedications(@RequestBody List<String> codes, @PathVariable String serialNumber) {
        Drone drone =
                droneService.loadMedications(codes, serialNumber);
        DroneDTO response = droneMapper.toLoadedDTO(drone);
        return ResponseEntity.ok().body(response);
    }
    @GetMapping("{serialNumber}/medications")
    public  ResponseEntity<?> getLoadedMedications(@PathVariable String serialNumber) {
        List<MedicationDTO> medicationDTOS = droneService.getLoadedMedications(serialNumber);
        return ResponseEntity.ok().body(medicationDTOS);
    }
    @GetMapping("{serialNumber}/batteryLevel")
    public  ResponseEntity<?> checkBatteryLevel(@PathVariable String serialNumber) {
        Drone drone = droneService.findBySerialNumber(serialNumber);
        return ResponseEntity.ok().body(drone.getBatteryCapacity());
    }
    @GetMapping
    public ResponseEntity<?> availableDrones(@RequestParam(defaultValue = "0") Integer page,
                                             @RequestParam(defaultValue = "15") Integer pageSize) {
        List<DroneSummaryDTO> drones = droneService.getAvailableDrones(page, pageSize);
        return ResponseEntity.ok().body(drones);
    }

    @PutMapping("{serialNumber}")
    public ResponseEntity<?> updateDrone(@PathVariable String serialNumber,
                                         @RequestBody @Valid DroneSummaryDTO droneSummaryDTO) {
        Drone updatedDrone = droneService.update(serialNumber, droneSummaryDTO);
        return ResponseEntity.ok(droneMapper.toDTO(updatedDrone));
    }
    @PutMapping("{serialNumber}/loading")
    public ResponseEntity<?> setTobeLoaded(@PathVariable String serialNumber) {
        Drone updatedDrone = droneService.setToBeLoaded(serialNumber);
        return ResponseEntity.ok(droneMapper.toDTO(updatedDrone));
    }
}
