package com.mbadr.drone.delivery.api.service;

import com.mbadr.drone.delivery.api.dto.DroneSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Drone;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DroneService {

    List<DroneSummaryDTO> getAvailableDrones(int page, int pageSize);

    Drone registerDrone(DroneSummaryDTO droneSummaryDTO);

    Drone loadMedications(List<String> medications, String serialNumber);

    Drone update(String serialNumber, DroneSummaryDTO droneDTO);

    Drone setToBeLoaded(String serialNumber);

    Drone findBySerialNumber(String serialNumber);

    List<MedicationDTO> getLoadedMedications(String serialNumber);
}
