package com.mbadr.drone.delivery.api.service.exceptions;

public class ResourceNotFoundException extends RuntimeException {

    private final String resourceName;
    private final String identity;

    public ResourceNotFoundException(String resourceName, String identity) {
        this.resourceName = initializeOrSetDefault(resourceName);
        this.identity = identity;
    }

    private String initializeOrSetDefault(String resourceName) {
        return "".equals(resourceName.trim()) ? "Resource" : resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getIdentity() {
        return identity;
    }

    @Override
    public String getMessage() {
        return this.getResourceName() + " (" + this.getIdentity() + ") is not found";
    }

}

