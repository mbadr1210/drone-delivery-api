package com.mbadr.drone.delivery.api.dto;


public class ImageSummaryDTO {

    private String fileType;
    private Long size;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

}
