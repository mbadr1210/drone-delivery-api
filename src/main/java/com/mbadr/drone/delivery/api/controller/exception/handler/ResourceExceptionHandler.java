package com.mbadr.drone.delivery.api.controller.exception.handler;

import com.mbadr.drone.delivery.api.dto.error.ExceptionDTO;
import com.mbadr.drone.delivery.api.service.exceptions.ConflictUpdateException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceDuplicationException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceNotFoundException;
import com.mbadr.drone.delivery.api.service.exceptions.ImageTransferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ResourceExceptionHandler {

    Logger logger = LoggerFactory.getLogger(ResourceExceptionHandler.class);
    public static final String RESOURCE_VALIDATION_ERROR_MESSAGE = "Resource Validation Error";

    /**
     * handling validation errors {@link javax.validation.Valid}
     * @param exception
     * @return {@link ExceptionDTO} with message and found errors
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionDTO> handleValidationExceptions(MethodArgumentNotValidException exception) {

        ExceptionDTO exceptionDTO = new ExceptionDTO();
        Map<String, String> errors = new HashMap<>();

        exception.getBindingResult()
                .getAllErrors()
                .forEach((error) -> {
                    String fieldName = ((FieldError) error).getField();
                    String errorMessage = error.getDefaultMessage();
                    errors.put(fieldName, errorMessage);
                });

        exceptionDTO.setMessage(RESOURCE_VALIDATION_ERROR_MESSAGE);
        exceptionDTO.setErrors(errors);

        logger.error("handleValidationExceptions()", exception);
        return ResponseEntity.badRequest().body(exceptionDTO);
    }

    /**
     * handles resource duplication exceptions
     * @param exception
     * @return
     */
    @ExceptionHandler({ResourceDuplicationException.class})
    public ResponseEntity<?> handleDuplicationExceptions(ResourceDuplicationException exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();
        exceptionDTO.setMessage(exception.getMessage());
        logger.error("handleDuplicationExceptions()", exception);
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .header("location", ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(exception.getId())
                        .toUriString())
                .body(exceptionDTO);
    }

    /**
     * handling not found Resources exceptions
     * @param exception
     * @return
     */
    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();
        exceptionDTO.setMessage(exception.getMessage());

        logger.error("handleResourceNotFoundException() ", exception);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
    }

    /**
     * handle conflict update exceptions
     * @param exception
     * @return
     */
    @ExceptionHandler({ConflictUpdateException.class})
    public ResponseEntity<?> handleConflictUpdateException(ConflictUpdateException exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();
        exceptionDTO.setMessage(exception.getMessage());
        exceptionDTO.setErrors(exception.getErrors());

        logger.error("handleConflictUpdateException() ", exception);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(exceptionDTO);
    }
    /**
     * handle upload image exception
     * @param exception
     * @return
     */
    @ExceptionHandler({ImageTransferException.class})
    public ResponseEntity<?> handleImageException(ImageTransferException exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO();
        exceptionDTO.setMessage("Couldn't Transfer image for medication (" + exception.getCode() + ")");
        exceptionDTO.setErrors(exception.getErrors());

        logger.error("handleImageException() ", exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exceptionDTO);
    }
}
