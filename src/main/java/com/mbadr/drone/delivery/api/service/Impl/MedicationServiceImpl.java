package com.mbadr.drone.delivery.api.service.Impl;

import com.mbadr.drone.delivery.api.dto.ImageResponse;
import com.mbadr.drone.delivery.api.dto.ImageSummaryDTO;
import com.mbadr.drone.delivery.api.dto.MedicationDTO;
import com.mbadr.drone.delivery.api.entity.Image;
import com.mbadr.drone.delivery.api.entity.ImageFactory;
import com.mbadr.drone.delivery.api.entity.Medication;
import com.mbadr.drone.delivery.api.mapper.MedicationMapper;
import com.mbadr.drone.delivery.api.repository.MedicationRepository;
import com.mbadr.drone.delivery.api.service.MedicationService;
import com.mbadr.drone.delivery.api.service.UploadingService;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceDuplicationException;
import com.mbadr.drone.delivery.api.service.exceptions.ResourceNotFoundException;
import com.mbadr.drone.delivery.api.service.exceptions.ImageTransferException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Service
public class MedicationServiceImpl implements MedicationService {
    private final MedicationRepository medicationRepository;

    private MedicationMapper medicationMapper;
    @Autowired
    private UploadingService uploadingService;


    @Autowired
    public void setMedicationMapper(MedicationMapper medicationMapper) {
        this.medicationMapper = medicationMapper;
    }

    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public ImageResponse getMedicationImage(String code) {
        Image image = getImage(code);
        return new ImageResponse(
                ImageFactory.getMediaType(image.getFileType()),
                getImageBytes(image)
        );
    }

    private Image getImage(String code) {
        Image image = findByCode(code).getImage();
        if (image == null)
            throw new ResourceNotFoundException("Image", code);
        return image;
    }

    private byte[] getImageBytes(Image image) {
        try {
            return uploadingService.download(image);
        } catch (IOException exception) {
            throw new ImageTransferException(image.getMedication().getCode(),
                    new HashMap<String, String>() {{
                        put("download", "couldn't download image");
                    }});
        }
    }

    @Override
    @Transactional
    public Medication registerMedication(MedicationDTO medicationDTO) {
        if (medicationRepository.existsByCode(medicationDTO.getCode())) {
            throw new ResourceDuplicationException(MedicationDTO.MEDICATION, medicationDTO.getCode());
        }
        Medication medication = medicationMapper.toEntity(medicationDTO);
        return medicationRepository.save(medication);

    }

    @Override
    public ImageSummaryDTO uploadImage(MultipartFile file, String code) {

        Image image = ImageFactory.buildImage(file);
        Medication medication = findByCode(code);
        image.setMedication(medication);

        uploadImage(file, image);

        return uploadingService.saveImage(image);
    }

    private void uploadImage(MultipartFile file, Image image) {
        try {
            uploadingService.upload(file, image);
        } catch (IOException exception) {
            throw new ImageTransferException(image.getMedication().getCode(), new HashMap<String, String>() {{
                put("upload", "couldn't upload image");
            }});
        }
    }


    @Override
    @Transactional(readOnly = true)
    public Medication findByCode(String code) {
        return medicationRepository.findByCode(code)
                .orElseThrow(() -> new ResourceNotFoundException(MedicationDTO.MEDICATION, code));
    }

    @Override
    public List<Medication> findAllByCodeIn(List<String> medicationsCodes) {
        return medicationRepository.findAllByCodeIn(medicationsCodes);
    }
}
