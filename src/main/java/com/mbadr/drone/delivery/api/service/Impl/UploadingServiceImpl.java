package com.mbadr.drone.delivery.api.service.Impl;

import com.mbadr.drone.delivery.api.dto.ImageSummaryDTO;
import com.mbadr.drone.delivery.api.entity.Image;
import com.mbadr.drone.delivery.api.mapper.ImageMapper;
import com.mbadr.drone.delivery.api.repository.ImageRepository;
import com.mbadr.drone.delivery.api.service.UploadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class UploadingServiceImpl implements UploadingService {
    @Value("${image.path:images}")
    private String imagesPath;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private ImageMapper imageMapper;

    private File imagesDirectory;

    @PostConstruct
    void init() {
        imagesDirectory = new File(imagesPath);
        if (!imagesDirectory.exists()) {
            imagesDirectory.mkdirs();
        }
    }

    @Override
    public void upload(MultipartFile imageMultiPart, Image image) throws IOException {
        File saveFile = new File(generateImageSavingPath(image));

        imageMultiPart.transferTo(saveFile);
    }

    @Override
    public byte[] download(Image image) throws IOException {
        File savedImageFile = new File(generateImageSavingPath(image));

        return Files.readAllBytes(Paths.get(savedImageFile.getAbsolutePath()));
    }

    @Override
    @Transactional
    public ImageSummaryDTO saveImage(Image image) {
        image = imageRepository.save(image);
        return imageMapper.toDTO(image);
    }

    private String generateImageSavingPath(Image image)
            throws IOException {
        return Paths.get(imagesDirectory.getCanonicalFile().getAbsolutePath(),
                image.getMedication().getCode() + "." + image.getFileType()).toString();
    }

}

