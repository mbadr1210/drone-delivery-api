package com.mbadr.drone.delivery.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DroneDeliveryApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DroneDeliveryApiApplication.class, args);
    }

}
